#!/usr/bin/env tclsh
package require xmlrpc

xmlrpc::serve 5555

proc test {a b} {
	return [list string $a$b]
}

vwait forever
