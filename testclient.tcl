#!/usr/bin/env tclsh

package require xmlrpc
if {[catch {set res [xmlrpc::call "http://localhost:5555" "test" {{int 1} {string b}}]}]} {
	puts "xmlrpc call failed"
} else {
	puts "res: $res."
}
